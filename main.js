const parseInput = require("./parseInput");
const writeOutput = require("./writeOutput");
const scan = require("./scan");
const brute = require("./brute");

var fileNames = [
  "a_example",
  "b_small",
  "c_medium",
  "d_quite_big",
  "e_also_big"
];

for (var i = 0; i < fileNames.length; i++) {
  var fileName = fileNames[i];
  var input = parseInput(fileName);

  ////

  var n = Math.min(input.slicesCounts.length, 17);
  var slicesToBrute = input.slicesCounts.slice(0, n);
  var slicesToScan = input.slicesCounts.slice(n);

  // SCAN
  var scanResult = scan(slicesToScan, input.maxSlices);
  var takenPizzasScan = scanResult.takenPizzas;
  var remaining = scanResult.remaining;

  // BRUTE
  var bruteResult = brute(slicesToBrute, remaining);
  var takenPizzasBrute = bruteResult;

  var merge = takenPizzasBrute.concat(takenPizzasScan);

  let pizzaIndexes = [];
  for (let i = 0; i < merge.length; i++) {
    if (merge[i] == 1) {
      pizzaIndexes.push(i);
    }
  }

  let out = {
    pizzaCount: eval(merge.join("+")),
    pizzaIndexes: pizzaIndexes
  };

  ////

  writeOutput(fileName, out);
}

console.log("fine.");
