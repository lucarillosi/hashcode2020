function brute(slicesToBrute, remaining) {
  if (slicesToBrute.length === 0) {
    return [];
  }

  var bestIndexes = new Array(slicesToBrute.length).fill("0");
  var bestSumOfSlices = 0;

  var counter = 0;
  var maxCounter = Math.pow(2, slicesToBrute.length);

  for (; counter < maxCounter; counter++) {
    // Converto counter in bin
    var indexes = counter.toString(2).split("");
    indexes = new Array(slicesToBrute.length - indexes.length)
      .fill("0")
      .concat(indexes);

    // Prendo le slices corrispondenti alla combinazione
    var tookSlices = slicesToBrute.filter((item, i) => indexes[i] === "1");

    // Vedo a quanto ammontano
    var sumOfSlices = eval(tookSlices.join("+"));
    if (sumOfSlices === undefined) sumOfSlices = 0;

    if (sumOfSlices <= remaining && sumOfSlices > bestSumOfSlices) {
      // Trovata combinazione migliore
      bestSumOfSlices = sumOfSlices;
      bestIndexes = indexes;
    }
  }

  return bestIndexes;
}

module.exports = brute;
