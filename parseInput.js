const fs = require("fs");

function parseInput(fileName) {
  var fileContent = fs.readFileSync("in/" + fileName + ".in", "ascii");

  var lines = fileContent.split("\n");

  var input = {
    maxSlices: parseInt(lines[0].split(" ")[0]),
    slicesCounts: lines[1].split(" ").map(n => parseInt(n))
  };

  return input;
}

module.exports = parseInput;
