function scan(slicesToScan, maxSlices) {
  var remaining = maxSlices;
  var takenPizzas = new Array(slicesToScan.length).fill(0);

  for (var i = slicesToScan.length - 1; i >= 0; i--) {
    let currentSlices = slicesToScan[i];

    if (currentSlices <= remaining) {
      // pizza in considerazione rientra nella slices disponibili
      takenPizzas[i] = 1;
      remaining -= currentSlices;
    }
  }

  return { takenPizzas: takenPizzas, remaining: remaining };
}

module.exports = scan;
