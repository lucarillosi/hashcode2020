const fs = require("fs");

function writeOutput(fileName, out) {
  var content = out.pizzaCount + "\n";

  content += out.pizzaIndexes.join(" ");

  fs.writeFileSync("out/" + fileName + ".out", content);
}

module.exports = writeOutput;
